package com.example.apiusers.Controller;

import com.example.apiusers.entity.User;
import com.example.apiusers.modelDTO.UserDTO;
import com.example.apiusers.service.Userservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller

public class UserController {
    @Autowired
    private Userservice userservice;
    @GetMapping("/user")
    public ResponseEntity<?> getListUser(){
        List<UserDTO> users= userservice.getListUser();
        return ResponseEntity.ok(users);
    }
    @PostMapping("/user")
    public ResponseEntity<?> creatUser(){
        return null;
    }
    @PutMapping("/user/{id}")
    public ResponseEntity<?> updateUser(){
        return null;
    }
    @DeleteMapping("/user/{id}")
    public ResponseEntity<?> DeleteUser(){
        return null;
    }

}
