package com.example.apiusers.service;

import com.example.apiusers.entity.User;
import com.example.apiusers.modelDTO.UserDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface Userservice {
    List<UserDTO> getListUser();
}
