package com.example.apiusers.service;

import com.example.apiusers.entity.User;
import com.example.apiusers.mapper.UserMapper;
import com.example.apiusers.modelDTO.UserDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserServiceImp implements Userservice{
    private static ArrayList<User> users= new ArrayList<User>();

    static {
        users.add(new User(1,"Nguyễn Thị Mông Mơ", "mongmo21@gmail.com", "0987654321","chandung.img","12345"));
        users.add(new User(2,"Nguyễn Văn Đức", "nguyenduc99th@gmail.com", "0989105386", "anh.img","123456"));
        users.add(new User(3,"Ngô Bình Dương", "ngobinhduong123@gmail.com", "0989105386", "anh.img","123456"));
        users.add(new User(4,"Phạm Đức Thắng", "nguyenvand@gmail.com", "0989105386", "anh.img","123456"));
        users.add(new User(5,"Phạm Văn Đồng ", "nguyen@gmail.com", "0989105386", "anh.img","123456"));


    }

    @Override
    public List<UserDTO> getListUser() {
        List<UserDTO> result = new ArrayList<UserDTO>();
        for(User user : users){
            result.add(UserMapper.touserDTO(user));
        }
        return result;
    }
}
